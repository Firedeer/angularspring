import { Component } from '@angular/core';
import { AuthService } from '../usuarios/auth.service';
import { Router } from '@angular/router';
import swal   from 'sweetalert2';
@Component(
    {
        selector: 'app-header',
        templateUrl: './header.component.html',
    }
)

export class HeaderComponent
{

    constructor(private router:Router,public authservice:AuthService)
    {}


logout():void{

    let usuario_=this.authservice.usuario.username;
    this.authservice.logout();
    swal.fire('Login',`Hasta luego ${usuario_}`,'success');
    this.router.navigate(['/login']);
 
}

}