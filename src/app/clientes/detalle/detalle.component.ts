import { Component, OnInit, Input } from '@angular/core';
import { Cliente } from '../cliente';
import {ClienteService} from '../cliente.service';
import {Router } from '@angular/router';
import swal   from 'sweetalert2';
import {ModalService} from './modal.service';
import { HttpEventType } from '@angular/common/http';
import { AuthService } from 'src/app/usuarios/auth.service';
import { Factura } from 'src/app/facturas/models/factura';
import { FacturaService } from 'src/app/facturas/services/factura.service';
@Component({
  selector: 'detalle-cliente',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.css']
})
export class DetalleComponent implements OnInit {

  @Input() cliente: Cliente;
  titulo:string = "Detalle del cliente";
  private fotoseleccionada:File;
  progreso:number=0;

 


  constructor( private clienteService: ClienteService, 
    public modalService:ModalService,
    private router: Router,
    public authservice:AuthService,
    private facturaService:FacturaService,
    ) { 

  }

  ngOnInit() {

    /* this.activatedRote.paramMap.subscribe(params=>{
      let id:number=+ params.get('id');

      if(id)
      {
        this.clienteService.getCliente(id).subscribe(cliente =>{
          this.cliente = cliente;
        });
      }
    }); */


  }
  seleccionarFoto(event)
  {

    this.fotoseleccionada =  event.target.files[0];
    this.progreso=0;
    console.log(this.fotoseleccionada);
    if(this.fotoseleccionada.type.indexOf('image')<0)
    {
      swal.fire('Error tipo de imagen',` Debe seleccionar una imagen`,'error');
      this.fotoseleccionada=null;
    }

    
  }
  subirFoto()
  {
    if(!this.fotoseleccionada)
    {
      swal.fire('Error al cargar foto',` Debe seleccionar una imagen`,'error');
    }
    else{
      this.clienteService.subirFoto(this.fotoseleccionada,this.cliente.id)
      .subscribe(event=>
        {
          if(event.type===HttpEventType.UploadProgress)
          {
            this.progreso= Math.round(100*event.loaded/event.total)
          }
          else if(event.type===HttpEventType.Response)
          {
            let response:any = event.body;
            this.cliente= response.cliente as Cliente;

            this.modalService.notificarUpload.emit(this.cliente);
            swal.fire('la foto se ha subido con exito',response.mensaje,'success');
          }
         // this.cliente=cliente;

        
          //this.router.navigate(['/clientes']);
        })
    }
  

  }

  cerrarModal()
  {
    this.fotoseleccionada=null;
    this.progreso=0;
    this.modalService.cerrarModal();
  }

  delete(factura: Factura): void
  {
    swal.fire({
      title: 'Esta seguro?',
      text: `Seguro que desea eliminar la factura ${factura.descripcion}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Si, Eliminarlo!',
      cancelButtonText: 'No, Cancelar'
    }).then((result) => {
      if (result.value) {
        this.facturaService.delete(factura.id).subscribe(
          response=>{
            this.cliente.facturas=this.cliente.facturas.filter( cli=> cli !== factura)
            swal.fire(
              'Factura eliminada!',
              `Factura numero ${factura.id} eliminado con exito.`,
              'success'
            )
          }
        )
        
      }
    })
  }
}
