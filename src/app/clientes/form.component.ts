import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import {Router, ActivatedRoute} from '@angular/router';
import swal   from 'sweetalert2';
import { Region } from './region';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public cliente: Cliente = new Cliente();

  private titulo: string = " Crear cliente";
  private errores: string[];
  private regiones: Region[];
  constructor( private clienteservice: ClienteService , 
    private router: Router,
    public activerouter: ActivatedRoute) { }

  ngOnInit() {
    this.cargarcliente();
    this.clienteservice.getRegiones().subscribe(regiones=> this.regiones= regiones);
  }
   
  cargarcliente(): void{

    this.activerouter.params.subscribe(params=>{

      let id = params['id']
      if(id)
      {
        this.clienteservice.getCliente(id).subscribe((cliente)=>this.cliente=cliente)
      }
    })

 

  }
  create(): void
  {
    this.clienteservice.create(this.cliente)
    .subscribe(cliente=>{
      this.router.navigate(['/clientes'])
      swal.fire('Nuevo cliente',`El cliente ${cliente.nombre} ha sido creado con exito`, 'success')
    },
    err=>
    {
      this.errores = err.error.error as string[];
      console.error('codigo de error => '+err.status);
      console.error(err.error.error);
    } 
    );
  }

  update():void
  {
    this.cliente.facturas=null;
    console.log("clientes actualizar",this.cliente);
    this.clienteservice.update(this.cliente)
    .subscribe(json=>{
      
      this.router.navigate(['/clientes']);

     swal.fire('Cliente Actualizado',`${json.mensaje} : ${json.cliente.nombre}`, 'success')
    })
  }

  compararRegion(c1: Region, c2: Region): boolean {
    if(c1===undefined && c2===undefined )
    {
      return true;
    }
     return c1==null || c2==null || c1==undefined || c2==undefined ? false: c1.id==c2.id;
   /*  return c1 && c2 ? c1.id === c2.id : c1 === c2; */
}

  

}
