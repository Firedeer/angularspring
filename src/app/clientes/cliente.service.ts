import { Injectable } from '@angular/core';
import {formatDate, DatePipe} from '@angular/common';
import { Cliente} from './cliente';

import {Observable,of, throwError} from 'rxjs';
import {HttpClient,HttpHeaders, HttpRequest, HttpEvent} from '@angular/common/http';
import {map, catchError,tap} from 'rxjs/operators';

import {Router} from '@angular/router';
import { Region } from './region';
import { AuthService } from '../usuarios/auth.service';

@Injectable({
  providedIn: 'root'
})


export class ClienteService {

  private urlcliente:string = 'http://localhost:8193/api/clientes';



  private httpHeaders= new HttpHeaders({'Content-Type':'application/json'})
  constructor(private http:HttpClient, private router:Router, private authservice:AuthService) { }

/*   private agregarCabecera(){

    let token=this.authservice.token;
    if(token!=null)
    {
      return this.httpHeaders.append('Authorization','Bearer '+token);
    }
    return this.httpHeaders;
    

  } */





  getClientes(page: number): Observable<any>
  {
    //return of(CLIENTES);
    return this.http.get(this.urlcliente+'/page/'+page).pipe(
      tap((response: any)=>{

        //console.log('cliente tab 1 ',response.content);
        //let clientes=response as Cliente[];
        (response.content as Cliente[]).forEach(cliente=>{
         // console.log(cliente.nombre);
        })
      }),
      map((response : any)=> {
          (response.content as Cliente[]).map( cliente=>{

          cliente.nombre= cliente.nombre.toUpperCase();
         // cliente.apellido= cliente.apellido.toUpperCase();
          //let datepipe= new DatePipe('es-PA');
         // cliente.createAt =datepipe.transform(cliente.createAt, 'EEEE dd, MMMM yyyy');//datepipe.transform(cliente.createAt, 'dd/MM/yyyy'); //formatDate(cliente.createAt, 'dd-MM-yyyy','en-US');
          return cliente;
        });
        return response;
      }) 
    );
  }

  create(cliente: Cliente) : Observable<Cliente>
  {
    //console.log(cliente);
    return  this.http.post(this.urlcliente,cliente).pipe(
        map((response: any)=> response.cliente as Cliente),
      catchError(e => {

        if(e.status==400)
        {
            return throwError(e);
        }
      /*   swal.fire(
          'Error al crear cliente', e.error.mensaje, 'error'
        )
        return throwError(e); */
      })
    );
  }

  getCliente(id): Observable<Cliente>{
   
    return this.http.get<Cliente>(`${this.urlcliente}/${id}`/* ,{headers:this.agregarCabecera()} */).pipe(

      catchError(e => {

        if(e.status!=401 && e.error.mensaje)
        {
          
            console.error(e.error.mensaje);
          
          this.router.navigate(['/clientes']);
        }
        
      
        
        return throwError(e);
      })
    )

  }

  getRegiones(): Observable<Region[]>{
    return this.http.get<Region[]>(this.urlcliente+'/regiones'/* ,{headers:this.agregarCabecera()} */);
  }

  update(cliente: Cliente): Observable<any>{
    
    return this.http.put<any>(`${this.urlcliente}/${cliente.id}`,cliente/* ,{headers: this.agregarCabecera()} */).pipe(
      //map((response: any)=> response.cliente as Cliente),
      catchError(e => {

        if(e.status==400)
       {
        if(e.error.mensaje)
        {
          console.error(e.error.mensaje);
        }
          return throwError(e);
       }
      /*   swal.fire(
          'Error al actualizar cliente', e.error.mensaje, 'error'
        )
        return throwError(e); */
      })
    );
  }

  delete(id: number): Observable<Cliente>{
    return this.http.delete<Cliente>(`${this.urlcliente}/${id}`/* ,{headers: this.agregarCabecera()} */).pipe(
      catchError(e=>{
        if(e.error.mensaje)
        {
          console.error(e.error.mensaje);
        }
        return throwError(e);
      })
    );
  }

  subirFoto(archivo: File, id): Observable<HttpEvent<{}>>
  {

    
    let formData= new FormData();
    formData.append("archivo", archivo);
    formData.append("id", id);


   /*  let httpHeaders = new HttpHeaders();
    let token = this.authservice.token;
    if (token != null) {
      httpHeaders = httpHeaders.append('Authorization', 'Bearer ' + token);
    } */

    const req = new HttpRequest('POST',`${this.urlcliente}/upload/`,formData, {
      reportProgress: true,
      /* headers:httpHeaders */
    });
    
    return this.http.request(req);

    
      /* map((response:any)=>response.cliente as Cliente),
      catchError(e => {
      
        console.error(e.error.mensaje);
        swal.fire(
          'Error', e.error.mensaje, 'error'
        )
        return throwError(e);
      })
 */
    
  }





}
