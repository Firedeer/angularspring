import { Component, OnInit } from '@angular/core';
import { Cliente} from './cliente';
import {ModalService} from './detalle/modal.service'
import { ClienteService} from './cliente.service';
import {tap} from 'rxjs/operators';
import swal   from 'sweetalert2';
import {ActivatedRoute} from '@angular/router';
import { AuthService } from '../usuarios/auth.service';
@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {


  clientes: Cliente[];
  paginator: any;
  clienteSeleccionado:Cliente;




  constructor(private ClienteService:ClienteService,
    private activerouter: ActivatedRoute,
     public modalService:ModalService,
     public authservice:AuthService ) { }

  ngOnInit(){
    this.activerouter.paramMap.subscribe(params=>{

      let page:number = +params.get('page');
      if(!page)
      {
        page=0;
      }
      this.ClienteService.getClientes(page)
      .pipe(
        tap(response => {
          //console.log('cliente tab 3');
           (response.content as Cliente[]).forEach(cliente=>{
  
           // console.log("get cliente ,", cliente);
          }) 
        })
      )
      .subscribe(response=>{
        this.clientes= (response.content as Cliente[]);
        this.paginator=response

      });
    });

    this.modalService.notificarUpload.subscribe(cliente=>{
      this.clientes=this.clientes.map(clienteOriginal=>{
        if(cliente.id==clienteOriginal.id)
        {
          clienteOriginal.foto=cliente.foto;
        }
        return clienteOriginal;
      })
    });

   
  }


  delete(cliente: Cliente): void
  {
    swal.fire({
      title: 'Esta seguro?',
      text: `Seguro que desea eliminar al cliente ${cliente.nombre} ${cliente.apellido}?`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#d33',
      cancelButtonColor: '#3085d6',
      confirmButtonText: 'Si, Eliminarlo!',
      cancelButtonText: 'No, Cancelar'
    }).then((result) => {
      if (result.value) {
        this.ClienteService.delete(cliente.id).subscribe(
          response=>{
            this.clientes=this.clientes.filter( cli=> cli !== cliente)
            swal.fire(
              'Cliente eliminado!',
              `Cliente ${cliente.nombre} eliminado con exito.`,
              'success'
            )
          }
        )
        
      }
    })
  }
  modalimagen(cliente: Cliente)
  {
    this.clienteSeleccionado=cliente;
    this.modalService.abrilModal();
  }
 
 
}
