import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { Factura } from '../models/factura';
import { Producto } from '../models/producto';

@Injectable({
  providedIn: 'root'
})

export class FacturaService {

  private urlEndpoint:string='http://localhost:8193/api/facturas';
  constructor( private http:HttpClient ) {}


  getFacturas(id: number): Observable<Factura>{

    return this.http.get<Factura>(`${this.urlEndpoint}/${id}`);
  }

  delete(id:number):Observable<void>{

    return this.http.delete<void>(`${this.urlEndpoint}/${id}`);
  }

  filtrarProducto(term:string):Observable<Producto[]>{

    return this.http.get<Producto[]>(`${this.urlEndpoint}/filtrar-productos/${term}`);
  }

  crearFactura(factura:Factura):Observable<Factura>{

    console.log(JSON.stringify(factura));
    return  this.http.post<Factura>(this.urlEndpoint,factura);
  }



}
