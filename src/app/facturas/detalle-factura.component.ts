import { Component, OnInit } from '@angular/core';
import {FacturaService} from './services/factura.service'
import {Factura} from './models/factura'
import {ActivatedRoute} from '@angular/router'
@Component({
  selector: 'app-detalle-factura',
  templateUrl: './detalle-factura.component.html',
  styleUrls: ['./detalle-factura.component.css']
})
export class DetalleFacturaComponent implements OnInit {

  constructor(private facturaService:FacturaService,
    private activateRoute:ActivatedRoute) { }

  factura: Factura;
  titulo:string="Detalle de factura";
  ngOnInit() 
  {
    this.activateRoute.paramMap.subscribe(params=>{
      let id=+params.get('id');
      this.facturaService.getFacturas(id).subscribe(factura=>{
        this.factura=factura;
      })
    })
  }




}
