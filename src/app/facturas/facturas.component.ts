import { Component, OnInit } from '@angular/core';
import { ClienteService } from '../clientes/cliente.service';
import { Factura } from './models/factura';
import { ActivatedRoute, Router } from '@angular/router';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith, flatMap} from 'rxjs/operators';
import { FacturaService } from './services/factura.service';
import { Producto } from './models/producto';
import { MatAutocompleteSelectedEvent } from '@angular/material';
import { ItemFactura } from './models/item-factura';
import { EventEmitter } from 'protractor';
import swal   from 'sweetalert2';
@Component({
  selector: 'app-facturas',
  templateUrl: './facturas.component.html',
  styleUrls: ['./facturas.component.css']
})
export class FacturasComponent implements OnInit {

  titulo:string="Nueva factura";
  factura:Factura=new Factura();

  AutocompletadoControl = new FormControl();
  ProductosFiltrados: Observable<Producto[]>;




  constructor(private clienteservice:ClienteService,
    private activateRoute:ActivatedRoute,
    private facturaService:FacturaService,
    private router:Router) { }

  ngOnInit()
   { 
     this.activateRoute.paramMap.subscribe(params=>{
    let id=+params.get('clienteId');
    this.clienteservice.getCliente(id).subscribe(cliente=>{
      this.factura.cliente=cliente;
    })
  })

  this.ProductosFiltrados = this.AutocompletadoControl.valueChanges
       .pipe(
        /* startWith(''), */
        map(value=>typeof value==='string'? value: value.nombre),
        flatMap(value => value ? this._filter(value):[]
        )
      ); 


  }

  private _filter(value: string): Observable<Producto[]> {
    const filterValue = value.toLowerCase();

    return this.facturaService.filtrarProducto(filterValue);
  }

  mostrarNombre(producto ? :Producto): string | undefined{
    return producto ? producto.nombre :  undefined ;
  }

  SeleccionarProducto(event: MatAutocompleteSelectedEvent): void{

    let producto=event.option.value as Producto;
    console.log(producto);

    if(this.existeItem(producto.id))
    {
      this.incrementarItem(producto.id)
    }
    else{
      let nuevoItem= new ItemFactura();
      nuevoItem.producto=producto;
      this.factura.items.push(nuevoItem);
    }
    

    this.AutocompletadoControl.setValue('');
    event.option.focus();
    event.option.deselect();

  }
  actualizarCantidad(id:number,event:any):void{

    let cantidad:number= event.target.value as number;

    if(cantidad<1)
    {
      return this.eliminarItem(id);

    }
    this.factura.items=this.factura.items.map((item:ItemFactura)=>
    {
      if(id===item.producto.id)
      {
        item.cantidad=cantidad;

      }
      return item;
    })
  }

  existeItem(id:number):boolean
  {
    let existe:boolean=false;

    this.factura.items.forEach((item:ItemFactura)=>
    {
      if(id=== item.producto.id)
      {
        existe=true;
      }
    })
    return existe;
  }
  incrementarItem(id:number):void
  {
    this.factura.items=this.factura.items.map((item:ItemFactura)=>
    {
      if(id===item.producto.id)
      {
        ++item.cantidad;

      }
      return item;
    })

  }

  eliminarItem(id:number)
  {
    this.factura.items=this.factura.items.filter( (item:ItemFactura)=> id !== item.producto.id)
  }
  crearFactura(facturaForm):void{
   
    if( this.factura.items.length==0)
    {
      this.AutocompletadoControl.setErrors({'invalid':true});
    }
    if(facturaForm.form.valid && this.factura.items.length>0)
    {
          this.facturaService.crearFactura(this.factura).subscribe(factura=>{
        swal.fire(
          this.titulo,'Factura creada con exito', 'success'
        );
        this.router.navigate(['/facturas',factura.id]);
      });
    } 
  }


}
