import { Component, OnInit } from '@angular/core';
import { Usuario } from './usuario';
import swal   from 'sweetalert2';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';
import { JSDocTagName } from '@angular/compiler/src/output/output_ast';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  titulo:String="Por favor inicie sesion";

  usuario:Usuario;

  constructor(private router:Router, private authservice:AuthService) { 


    this.usuario= new Usuario();



  }


  ngOnInit() 
  {
    
    if(this.authservice.isAutenticado())
    {
      swal.fire('Login',`Hola ${this.authservice.usuario.username} ya estas autenticado`,'info');
      this.router.navigate(['/clientes']);
    }
  }

  login():void{

    if(this.usuario.username==null || this.usuario.password==null )
    {
    
    
      swal.fire('Error login','Username o password vacios','error');
      return;


    }



    this.authservice.login(this.usuario).subscribe(response=>
      {
      
      this.authservice.guardarUsuario(response.access_token);
      this.authservice.guardarToken(response.access_token);

      let usuario=this.authservice.usuario;
      this.router.navigate(['/clientes']);
      swal.fire('Login',`Bienvenido ${usuario.username}`,'success');
      },err=>{
        if(err.status==400)
        {
          swal.fire('Error Login','Usuario o clave incorrecta','error');
        }
      });




  }

}
