import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private authservice:AuthService,
     private router:Router){}


  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    
      if(this.authservice.isAutenticado())
      {
        if(this.IsTokenExpirado())
        {
          this.authservice.logout();
          this.router.navigate(['/login']);
          return false;
        }
        return true;
      }
      this.router.navigate(['/login']);
      return false;
  }
  IsTokenExpirado():boolean{
    let token= this.authservice.token;
    let payload= this.authservice.obtenerAccessToken(token);
    let now= new Date().getTime()/1000
    if(payload.exp<now)
    {
      return true;
    }
    return false;
  }
  
}
